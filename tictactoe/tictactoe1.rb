#!/usr/bin/ruby
#
# A Ruby Tic Tac Toe Implementaion
#
# Command line version for two players on the same computer

# Setup inital board
# -1 empty space
# 0 player O
# 1 player X
board=[[-1,-1,-1],
       [-1,-1,-1],
       [-1,-1,-1]]

# this will print the board as the game is played as shown below (initial display)
#   _ _ _
#   _ _ _
#   _ _ _ 
def print_board(board)
  puts "Board "
  (0..2).each do |j|
    (0..2).each do |i|
     if board[i][j] == 0
       print "O "
     elsif board[i][j] == 1
       print "X "
     else
        print "_ "
     end
    end
   print "\n"
  end

  return
end

# display the coordinates on the screen
# input of the coordinates by player
# check for valid moves

def got_valid_move?(board,player,move,mover)
  def get_coordinate(player,move,mover)
    if player == 1
      mover = "X"
    else
      mover = "O"
    end
    puts "\nmove #{move}"
    puts "Player #{mover}, please indicate the co-ordinates of your next move"
    puts "Input in the format x,y e.g. 1,1  "
    puts "[ (0,0) (1,0) (2,0) ]"
    puts "[ (0,1) (1,1) (2,1) ]"
    puts "[ (0,2) (1,2) (2,2) ]\n"

    puts "Input coordinates"

    coord = [-1,-1]
    coord = gets.chomp.to_s.split(",")
    x = coord[0].to_i
    y = coord[1].to_i
    xy = [x,y]
  end
  xy = get_coordinate(player,move,mover)
  x = xy[0].to_i; y = xy[1].to_i
  # validate the coordinate values
  def valid_coordinate?(x,y)
    return true unless !(x && y).is_a?(Numeric) || (x or y) > 2 || (x or y) > 2
  end
  # display the move by player
  if valid_coordinate?(x,y)
    puts "Player #{mover}: (#{x},#{y})\n"
  else
    puts "Invalid coordinate. Put number between 0 and 2"
    return false
  end

  # check whether the board space is empty

  if board[x][y] == -1
    board[x][y] = player
    return true
  else
    puts "Invalid move"
    return false
  end
end

def checkendgame(board, move)
  gamestatus = -1
  # Left to right, top to bottom diagonal
  if ((board[0][0]==board[1][1]) and (board[0][0]==board[2][2]) and (board[0][0]!=-1))
    gamestatus = board[0][0]
  end
 # Right to left, top to bottom diagonal
  if ((board[0][2]==board[1][1]) and (board[0][2]==board[2][0]) and (board[0][2]!=-1))
    gamestatus = board[0][2]
  end
  # Rows
  (0..2).each do |j|
    if ((board[0][j]==board[1][j]) and (board[0][j]==board[2][j]) and (board[0][j]!=-1))
      gamestatus = board[0][j]
    end
  end
  # Columns
  (0..2).each do |i|
    if ((board[i][0]==board[i][1]) and (board[i][0]==board[i][2]) and (board[i][0]!=-1))
      gamestatus = board[i][0]
    end
  end
  # Check for stalemate
  if ((move == 8) and gamestatus == -1)
    gamestatus = 2
  end
  return gamestatus 
 end

# Main program
#
# Variable to keep track of winner and whether game has ended
# -1 -> game not ended
# 0 -> Player 0 has won
# 1 -> Player 1 has won
# Any other value, draw
gamestatus = -1 
def endgame?(gamestatus,board)
  if gamestatus == -1
    return false
  else
    print_board(board)
    if gamestatus == 0
      puts "Mshindi ni O"
    elsif gamestatus == 1
      puts "Mshindi ni X"
    else
      puts "Hakuna mshindi"
    end
    return true
  end
end

# Main Game Loop
#
move = 0
# movesuccess = false
begin

  print_board(board)
  player = move%2
  mover = "0"
  movesuccess = false
  # Check for false moves. Keep trying
  begin  
    movesuccess = got_valid_move?(board,player,move,mover)
  end while !movesuccess
  gamestatus = checkendgame(board,move)
  
  move +=1 
end while !endgame?(gamestatus,board)

# Show the final configuration
#
puts "Asante kwa kucheza!"